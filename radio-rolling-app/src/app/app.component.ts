import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, LoadingController } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';

import { HomePage } from "../pages/home/home";
import { MusicPage } from "../pages/music/music";
import { TextPage } from "../pages/text/text";
import { FactsPage } from "../pages/facts/facts";
import { PapersPage } from "../pages/papers/paper";

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  loading:any;
  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public loadingCtrl: LoadingController
  ) {
    this.initializeApp();
    if (this.loading) {
      this.loading.dismiss();
    }
    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Music', component: MusicPage, icon: 'musical-notes'},
      {title: 'Text', component: TextPage, icon: 'planet'},
      //{title: 'Facts', component: FactsPage, icon: 'planet'}
      {title: 'Papers', component: PapersPage, icon: 'book'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar

      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
      var that = this;
      this.checkInternetConnection().subscribe((online: boolean) => {
        console.log('connection', online);
        if (that.loading) {
          that.loading.dismiss();
        }
        if (!online) {
          that.presentLoading('NO INTERNET CONNECTION! Connect to internet to use this app!');
            // this.notificationService.addNotification('common.warnings.upload-connection', 'warning');
          console.log("no connection");
        }
      });
    });
  }

  presentLoading(content) {
    this.loading = this.loadingCtrl.create({
      content: content
    });
    this.loading.present();
  }

  checkInternetConnection(): Observable<boolean> {
    return Observable.merge(
        Observable.fromEvent(window, 'offline').map(() => false),
        Observable.fromEvent(window, 'online').map(() => true),
        Observable.create(sub => {
            sub.next(navigator.onLine);
            sub.complete();
        })
    );
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.pauseVideo();
    if (page.component.name==="HomePage") {
      this.nav.setRoot(page.component);
    }
    else {
      this.nav.push(page.component);
    }
    
  }
  pauseVideo() {
    var frame = top.document.querySelector("#iframe12");
    if (frame!==null) {
      frame["contentWindow"].postMessage("stopit" ,'*');
    }
  }

}
