import {NgModule,Pipe} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import { HttpModule } from '@angular/http';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {MyApp} from "./app.component";
import {HomePage} from "../pages/home/home";
import {MusicPage} from "../pages/music/music";
import {TextPage} from "../pages/text/text";
import {FactsPage} from "../pages/facts/facts";
import {PapersPage} from "../pages/papers/paper";
import {PlaylistItems} from "../pages/popovers/playlist_items";
import { RestProvider } from '../providers/rest/rest';
import {NoSanitizePipe} from '../pipes/no_sanitize_pipe';
import { SocialSharing } from '@ionic-native/social-sharing';

// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MusicPage,
    TextPage,
    FactsPage,
    PapersPage,
    PlaylistItems,
    NoSanitizePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,    
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MusicPage,
    FactsPage,
    PapersPage,
    PlaylistItems,
    TextPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    RestProvider,
    SocialSharing,
    {provide: Window, useValue: window}
  ]
})

export class AppModule {
}
