import { Component,ViewChild } from '@angular/core';
import { NavController,Content, PopoverController,LoadingController,AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { YoutubeService } from '../../service/youtube.service';
import {PlayerService} from '../../service/player.service';
import {PlaylistItems} from '../../pages/popovers/playlist_items';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
// import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: 'music.html',
  providers:[YoutubeService,PlayerService]
})

export class MusicPage {
  @ViewChild(Content) content: Content;
  video: any;
  musicPlaylists: any;
  currentPlaylist: any;
  loading;
  url: any;
  playlist: any;
  vPlayer = true;
  videos = {};
  search = {
    params:''
  };

  private serverUrl = 'http://radiorolling.com/ws'
  private stompClient;

  constructor(public alertCtrl:AlertController, public loadingCtrl:LoadingController,public navParams:NavParams,
    public navCtrl: NavController, public popoverCtrl: PopoverController, public restProvider: RestProvider,public player:PlayerService, public youtubeService: YoutubeService) {
      
    this.playlist = {};
    this.playlist.id = 1;
    this.presentLoading();
    this.getMusicPlaylists(1);
    this.getCurrentPlaylist(1);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PlaylistItems,{playlist:this.currentPlaylist});
    popover.present({
      ev: myEvent
    });
    
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
       content: 'Please wait...'
    });

    this.loading.present();
  }

  dismissLoading(){
    var that = this;
    setTimeout(function(){
      that.getNowPlaying(1);
      that.initializeWebSocketConnection();
      that.loading.dismiss();
    }, 600);
    
  }

  getCurrentPlaylist(type) {
    this.restProvider.getCurrentPlaylist(type)
      .then(data => {
        this.currentPlaylist = data;
        this.loading.dismiss();
        console.log(this.currentPlaylist.videos[0]);
      });
  }

  getMusicPlaylists(type) {
    this.restProvider.getPlaylists(type)
      .then(data => {
        this.musicPlaylists = data;
        console.log(this.musicPlaylists);
      });
  }

  changeSong(id) {
    setTimeout(function(){
      var frame = top.document.querySelector("#iframe1");
      frame["contentWindow"].postMessage(id ,'*');
    }, 2600);
    
  }
  getNowPlaying(type) {
    this.restProvider.getNowPlaying(type)
      .then(data => {
        this.video = data;
        let yt_id = this.video.videoUrl;
        console.log(yt_id);
        this.changeSong(yt_id);
      });
  }

  presentAlert(t) {
    let alert = this.alertCtrl.create({
      title: t
    });
    alert.present();
  }
  changePlaylist(p) {
    this.presentLoading();
    this.stompClient.disconnect();
    this.playlist.id = p;
    this.getNowPlaying(p);
    this.initializeWebSocketConnection();
    this.getCurrentPlaylist(p);
    this.content.scrollTo(0,0,500);
  }
  getNowPlayingOnChange(type) {
    this.restProvider.getNowPlaying(type)
      .then(data => {
        this.video = data;
        let yt_id = this.video.videoUrl;
        console.log(yt_id);
        this.changeSong(yt_id);
      });
  }
  initializeWebSocketConnection() {

    let ws = new SockJS(this.serverUrl);

    this.stompClient = Stomp.over(ws);

    let that = this;

    this.stompClient.connect({},
      function (frame) {
        that.stompClient.subscribe("/channel/public/"+that.playlist.id, (message) => {
          if (message.body) {
            that.video = JSON.parse(message.body);
            let yt_id = that.video.videoUrl;
            console.log("YT"+yt_id);
            //that.playVideo(yt_id);
            that.changeSong(yt_id);
            
          }
        });
      });
  }
  
}
  