import { Component } from '@angular/core';
import { ViewController,NavParams,NavController,App } from 'ionic-angular';
import {MusicPage} from "../../pages/music/music";

@Component({
    template: `
      <ion-list>
        <ion-list-header>Pesme iz plejliste <b>{{currentPlaylist?.name}}</b></ion-list-header>
        <button *ngFor="let video of currentPlaylist?.videos" ion-item large>
                  <h3>{{video?.description}}</h3> 
        </button>
      </ion-list>
    `
  })
  
  export class PlaylistItems {
    currentPlaylist:any;
    constructor(public app:App,public viewCtrl: ViewController,public navParams:NavParams) {
        this.currentPlaylist = this.navParams.data.playlist;
        this.currentPlaylist.videos.sort(function(a, b) {
            return a.index_num - b.index_num;
        });
    }
  
    close() {
      this.viewCtrl.dismiss();
    }
  }