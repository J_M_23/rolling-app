import { Pipe,Component,ViewChild,ChangeDetectorRef } from '@angular/core';
import { NavController,Content, PopoverController,LoadingController,AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import {PlaylistItems} from '../../pages/popovers/playlist_items';
import $ from 'jquery';
import {NoSanitizePipe} from '../../pipes/no_sanitize_pipe';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as html2canvas from 'html2canvas';

@Component({
  templateUrl: 'paper.html'
})

export class PapersPage {
  @ViewChild(Content) content: Content;
  loading;
  isLoading:any;
  isNotVisible: any;
  helpDiv: any;
  sharingSource: any;
  papers : any;
  start: any;
  max: any;
  loaded: any;

  constructor(private socialSharing:SocialSharing, private changeDetectorRef: ChangeDetectorRef,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public navParams:NavParams,
    public navCtrl: NavController, public popoverCtrl: PopoverController, public restProvider: RestProvider) {
    this.sharingSource = null;
    this.isNotVisible = false;
    this.papers=[];
    this.loaded=false;
    this.isLoading = false;
    this.presentLoading();
    this.start=0;
    this.max = 10;
    this.getTextPlaylists();
  }
  

  ngAfterViewInit() {
    let that = this;
        this.content.ionScrollEnd.subscribe((data)=> {
            that.isNotVisible = $("#papers").offset().top < 0;
            that.changeDetectorRef.detectChanges();
        });
  }
  goToTop() {
    this.content.scrollTo(0,0,500);
  }
  dismissLoading() {
    this.loading.dismiss();
  }
  presentLoading() {
    this.loading = this.loadingCtrl.create({
       content: 'Please wait...'
    });
    this.loading.present();
  }

  getImageFromUrl(url) {
    this.restProvider.getImageFromUrl(url)
      .then(data => {
        this.sharingSource = data;
        this.changeDetectorRef.detectChanges();
        html2canvas(document.getElementById("shareimg")).then((canvas)=>{
            let imgData = canvas.toDataURL("image/PNG");
            console.log(imgData);
            this.socialSharing.share("RadioRolling", null, imgData, null);
            this.sharingSource = null;
            this.loading.dismiss();
          });
        
      });
  }

  getTextPlaylists() {
    this.isLoading = true;
    this.loaded = true;
    this.restProvider.getPapersInRange(14456,this.start,this.max)
      .then(data => {
        this.loaded = false;
        this.isLoading = false;
        this.papers.push.apply(this.papers, data);
        //this.papers = data; 
        this.start+=this.max;
        this.dismissLoading();
        this.changeDetectorRef.detectChanges();
        if (this.papers.length%this.max!=0) {
            this.loaded = true;
        }
      });
  }
  loadMore() {
    this.getTextPlaylists();
  }
  shareQ(j) {
      this.changeDetectorRef.detectChanges();
      this.presentLoading();
      this.getImageFromUrl("http://radiorolling.com"+this.papers[j].ytId);
  }

  presentAlert(t) {
    let alert = this.alertCtrl.create({
      title: t
    });
    alert.present();
  }

  appendImage(index) {
    this.helpDiv = $("#quote_"+index).text();
    this.changeDetectorRef.detectChanges();
  }

  
  
}
  