import { Pipe,Component,ViewChild,ChangeDetectorRef } from '@angular/core';
import { NavController,Content, PopoverController,LoadingController,AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import {PlaylistItems} from '../../pages/popovers/playlist_items';
import $ from 'jquery';
import {NoSanitizePipe} from '../../pipes/no_sanitize_pipe';
// import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: 'text.html'
})

export class TextPage {
  @ViewChild(Content) content: Content;
  text: any;
  textPlaylists: any;
  currentPlaylist: any;
  openPlaylistIndex: any;
  loading;
  url: any;
  playlist: any;
  lastSavedScrollTop: any;
  isNotVisible: any;

  constructor(private changeDetectorRef: ChangeDetectorRef,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public navParams:NavParams,
    public navCtrl: NavController, public popoverCtrl: PopoverController, public restProvider: RestProvider) {
    this.text = null;
    this.openPlaylistIndex = null;
    this.currentPlaylist = null;
    this.playlist = {};
    this.isNotVisible = false;
    
    this.presentLoading();
    this.getTextPlaylists(3);
    
  }

  onPageScroll(event) {
    //console.log(document.querySelector("#title_"+this.openPlaylistIndex).scrollTop - event.target.scrollTop);
  }

  ngAfterViewInit() {
    let that = this;
    this.content.ionScrollEnd.subscribe((data)=>{
        if (that.openPlaylistIndex!=null) {
            that.isNotVisible = $("#title_"+that.openPlaylistIndex).offset().top < 0;
            that.changeDetectorRef.detectChanges();
        }
        else {
            that.isNotVisible = false;
            that.changeDetectorRef.detectChanges();
        }
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PlaylistItems,{playlist:this.currentPlaylist});
    popover.present({
      ev: myEvent
    });
    
  }
  dismissLoading() {
    this.loading.dismiss();
  }
  presentLoading() {
    this.loading = this.loadingCtrl.create({
       content: 'Please wait...'
    });
    this.loading.present();
  }

  getCurrentPlaylist(type) {
    this.restProvider.getCurrentPlaylist(type)
      .then(data => {
        this.currentPlaylist = data;
        this.currentPlaylist.videos.sort(function(a, b) {
            return a.index_num - b.index_num;
        });
        this.loading.dismiss();
      });
  }

  getTextPlaylists(type) {
    this.restProvider.getPlaylists(type)
      .then(data => {
        this.textPlaylists = data;        
        this.dismissLoading();
      });
  }

  changePlaylist(p) {
    this.presentLoading();
    this.playlist.id = p;
    this.getCurrentPlaylist(p);
  }

  presentAlert(t) {
    let alert = this.alertCtrl.create({
      title: t
    });
    alert.present();
  }

  collapseAllExceptCurrent() {
    for (var i = 0;i<this.textPlaylists.length;i++) {
        if (this.textPlaylists[i].id!==this.currentPlaylist.id) {
            this.textPlaylists[i].open = false;    
        }
        else {
            this.textPlaylists[i].open = true;
            this.openPlaylistIndex = i;
        }
        }
  }

  toggleSection(i) {
    this.text = null;
    this.textPlaylists[i].open = !this.textPlaylists[i].open;
    this.currentPlaylist = this.textPlaylists[i];
    this.openPlaylistIndex = i;
    for (var j = 0;j<this.textPlaylists.length;j++) {
        if (j!=i) this.textPlaylists[j].open = false;
    }
    this.content.scrollTo(0,0,500);
    this.content.scrollTo(0, $("#title_"+this.openPlaylistIndex).offset().top,1000);
  }

  chooseText(id, playlist) {
    for (var i = 0;i<this.textPlaylists.length;i++) {
        if (this.textPlaylists[i].id===playlist) {
            this.currentPlaylist = this.textPlaylists[i];
            for (var j = 0;j<this.textPlaylists[i].videos.length;j++) {
                if (this.textPlaylists[i].videos[j].id===id) {
                    this.text = this.textPlaylists[i].videos[j];
                    this.text.quote = this.replaceAll(this.text.quote, "//www.youtube.com/embed/","https://www.youtube.com/embed/")
                    this.text.quote = this.replaceAll(this.text.quote, "src=\"radiorolling.com/res/","src=\"http://radiorolling.com/res/");
                    this.text.quote = this.replaceAll(this.text.quote, "src=\"/res/","src=\"http://radiorolling.com/res/");                                        
                    this.collapseAllExceptCurrent();
                    this.lastSavedScrollTop = this.content.scrollTop;
                    this.content.scrollToTop();
                    break;
                }
            }
            break;    
        }
    }
  }

  closeText() {
    this.text = null;
    this.content.scrollTo(0, this.lastSavedScrollTop,10);
  }

  closePlaylist() {
    this.textPlaylists[this.openPlaylistIndex].open = false;
    this.currentPlaylist = null;
    this.openPlaylistIndex = null;
    this.isNotVisible = false;
    this.changeDetectorRef.detectChanges();
    this.content.scrollToTop();
  }

  replaceAll (target, search, replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
  };
  
}
  