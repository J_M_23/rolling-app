import { Pipe,Component,ViewChild,ChangeDetectorRef } from '@angular/core';
import { NavController,Content, PopoverController,LoadingController,AlertController,NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import {PlaylistItems} from '../../pages/popovers/playlist_items';
import $ from 'jquery';
import {NoSanitizePipe} from '../../pipes/no_sanitize_pipe';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as html2canvas from 'html2canvas';
// import { HttpErrorResponse } from '@angular/common/http';

@Component({
  templateUrl: 'facts.html'
})

export class FactsPage {
  @ViewChild(Content) content: Content;
  text: any;
  quotes: any;
  currentQuote: any;
  textPlaylists: any;
  currentPlaylist: any;
  openPlaylistIndex: any;
  loading;
  url: any;
  playlist: any;
  lastSavedScrollTop: any;
  isNotVisible: any;
  helpDiv: any;
  shareQuote: any;
  sharingSource: any;
  logoSource: any;
  shareQuoteAuthor: any;

  constructor(private socialSharing:SocialSharing, private changeDetectorRef: ChangeDetectorRef,public alertCtrl:AlertController, public loadingCtrl:LoadingController,public navParams:NavParams,
    public navCtrl: NavController, public popoverCtrl: PopoverController, public restProvider: RestProvider) {
    this.text = null;
    this.sharingSource = null;
    this.logoSource = null;
    this.openPlaylistIndex = null;
    this.currentPlaylist = null;
    this.playlist = {};
    this.isNotVisible = false;
    this.helpDiv = null;
    this.shareQuote = null;
    this.shareQuoteAuthor = null;
    this.presentLoading();
    this.getTextPlaylists(6);
    this.getQuotes();
  }
  

  ngAfterViewInit() {
    let that = this;
    this.content.ionScrollEnd.subscribe((data)=>{
        if (that.openPlaylistIndex!=null) {
            that.isNotVisible = $("#title_"+that.openPlaylistIndex).offset().top < 0;
            that.changeDetectorRef.detectChanges();
        }
        else {
            that.isNotVisible = false;
            that.changeDetectorRef.detectChanges();
        }
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PlaylistItems,{playlist:this.currentPlaylist});
    popover.present({
      ev: myEvent
    });
    
  }
  dismissLoading() {
    this.loading.dismiss();
  }
  presentLoading() {
    this.loading = this.loadingCtrl.create({
       content: 'Please wait...'
    });
    this.loading.present();
  }

  getCurrentPlaylist(type) {
    this.restProvider.getCurrentPlaylist(type)
      .then(data => {
        this.currentPlaylist = data;
        this.currentPlaylist.videos.sort(function(a, b) {
            return a.index_num - b.index_num;
        });
        this.loading.dismiss();
      });
  }

  getImageFromUrl(url) {
    this.restProvider.getImageFromUrl(url)
      .then(data => {
        this.sharingSource = data;
        this.changeDetectorRef.detectChanges();
        html2canvas(document.getElementById("shareimg")).then((canvas)=>{
            let imgData = canvas.toDataURL("image/PNG");
            console.log(imgData);
            this.socialSharing.share("RadioRolling", null, imgData, null);
            this.helpDiv = null;
            this.loading.dismiss();
          });
        
      });
  }

  getLogoFromUrl() {
    this.restProvider.getImageFromUrl("http://radiorolling.com/res/images/20182101446/rolllogo.png")
      .then(data => {
        this.logoSource = data;
        this.changeDetectorRef.detectChanges();
        html2canvas(document.getElementById("shareimg2")).then((canvas)=>{
            let imgData = canvas.toDataURL("image/PNG");
            console.log(imgData);
            this.socialSharing.share("RadioRolling", null, imgData, null);
            this.shareQuote = null;
            this.shareQuoteAuthor = null;
            this.loading.dismiss();
          });
        
      });
  }

  getQuotes() {
    this.restProvider.getPlaylists(5)
      .then(data => {
        this.quotes = data[0]; 
        console.log(this.quotes);
        this.dismissLoading();
      });
  }

  getTextPlaylists(type) {
    this.restProvider.getPlaylists(type)
      .then(data => {
        this.textPlaylists = data; 
        for (var i = 0;i<this.textPlaylists.length;i++) {
            for (var j = 0;j<this.textPlaylists[i].videos.length;j++) {
                this.textPlaylists[i].videos[j].quote = this.replaceAll(this.textPlaylists[i].videos[j].quote, "//www.youtube.com/embed/","https://www.youtube.com/embed/")
                this.textPlaylists[i].videos[j].quote = this.replaceAll(this.textPlaylists[i].videos[j].quote, "src=\"radiorolling.com/res/","src=\"http://radiorolling.com/res/");
                this.textPlaylists[i].videos[j].quote = this.replaceAll(this.textPlaylists[i].videos[j].quote, "src=\"/res/","src=\"http://radiorolling.com/res/");                                        
                this.textPlaylists[i].videos[j].quote = this.replaceAll(this.textPlaylists[i].videos[j].quote, "<font color","<font id=\"quote_"+j+"\" color");
            }       
        }
        this.dismissLoading();
      });
  }
  shareQ(j) {
      this.shareQuoteAuthor = this.quotes.videos[j].description;
      this.shareQuote = "\""+this.quotes.videos[j].quote+"\"";
      this.changeDetectorRef.detectChanges();
      this.presentLoading();
      this.getLogoFromUrl();
  }
  share() {
      console.log("share");
  }
  changePlaylist(p) {
    this.presentLoading();
    this.playlist.id = p;
    this.getCurrentPlaylist(p);
  }

  presentAlert(t) {
    let alert = this.alertCtrl.create({
      title: t
    });
    alert.present();
  }

  collapseAllExceptCurrent() {
    this.quotes.open=false;
    for (var i = 0;i<this.textPlaylists.length;i++) {
        if (this.textPlaylists[i].id!==this.currentPlaylist.id) {
            this.textPlaylists[i].open = false;    
        }
        else {
            this.textPlaylists[i].open = true;
            this.openPlaylistIndex = i;
        }
        }
  }

  toggleSection(i) {
    this.text = null;
    this.textPlaylists[i].open = !this.textPlaylists[i].open;
    this.currentPlaylist = this.textPlaylists[i];
    this.openPlaylistIndex = i;
    for (var j = 0;j<this.textPlaylists.length;j++) {
        if (j!=i) this.textPlaylists[j].open = false;
    }
    this.content.scrollTo(0,0,500);
    this.content.scrollTo(0, $("#title_"+this.openPlaylistIndex).offset().top,1000);
  }

  toggleSectionQ() {
    this.text = null;
    this.quotes.open = !this.quotes.open;
    this.content.scrollTo(0,0,500);
    this.content.scrollTo(0, $("#quotes").offset().top,1000);
    if (this.quotes.open) {
        for (var i = 0;i<this.textPlaylists.length;i++) {
            this.textPlaylists[i].open = false;    
        }
    }
  }

  closePlaylist() {
    this.textPlaylists[this.openPlaylistIndex].open = false;
    this.currentPlaylist = null;
    this.openPlaylistIndex = null;
    this.isNotVisible = false;
    this.changeDetectorRef.detectChanges();
    this.content.scrollToTop();
  }

  replaceAll (target, search, replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
  };

  facebookShare(index){
    var msg  = "RadioRolling";
    this.socialSharing.shareViaFacebook(msg, null, null); 
  }

  regularShare(index){
    console.log(this.currentPlaylist.videos[index]);
    var msg = "RadioRolling";
    this.appendImage(index);
    
    this.presentLoading();
    this.getImageFromUrl("http://radiorolling.com/"+this.currentPlaylist.videos[index].ytId);
  }

  appendImage(index) {
    this.helpDiv = $("#quote_"+index).text();
    this.changeDetectorRef.detectChanges();
  }

  
  
}
  