import { Component,ChangeDetectorRef,ViewChild } from '@angular/core';
import {NavController, PopoverController,LoadingController,Content} from "ionic-angular";
import {Storage} from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as html2canvas from 'html2canvas';

@Component({
  templateUrl: 'home.html'
})

export class HomePage {
  @ViewChild(Content) content: Content;
  song: any;
  played:any;
  quote:any;
  text:any;
  sText:any;
  loading:any;
  shareQuote: any;
  logoSource: any;
  shareQuoteAuthor: any;
  
  constructor(private socialSharing:SocialSharing,private changeDetectorRef: ChangeDetectorRef,public loadingCtrl:LoadingController,private storage: Storage, public nav: NavController, public popoverCtrl: PopoverController, public restProvider: RestProvider) {
    this.presentLoading();
    this.played=false;
    this.shareQuote = null;
    this.logoSource = null;
    this.shareQuoteAuthor = null;
    this.getSong();
    this.getQuote();
    this.getText();
  }
  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: ''
    });
    this.loading.present();
  }
  getSong() {
    this.restProvider.getRecomended(1)
    .then(data => {
      this.song = data;
      console.log(this.song);
      this.loading.dismiss();
    });
  }
  getQuote() {
    this.restProvider.getRecomended(5)
    .then(data => {
      this.quote = data;
      console.log(this.quote);
    });
  }
  shareQ() {
    this.shareQuoteAuthor = this.quote.description;
    this.shareQuote = "\""+this.quote.quote+"\"";
    this.changeDetectorRef.detectChanges();
    this.presentLoading();
    this.getLogoFromUrl();
  }
  closeText() {
    this.sText = this.text;
    this.text = null;
    this.content.scrollTo(0,0,500);
  }
  openText() {
    this.text = this.sText;
  }
  getText() {
    this.restProvider.getRecomended(3)
    .then(data => {
      this.sText = data;
      this.sText.quote = this.replaceAll(this.sText.quote, "//www.youtube.com/embed/","https://www.youtube.com/embed/")
      this.sText.quote = this.replaceAll(this.sText.quote, "src=\"radiorolling.com/res/","src=\"http://radiorolling.com/res/");
      this.sText.quote = this.replaceAll(this.sText.quote, "src=\"/res/","src=\"http://radiorolling.com/res/");
      console.log(this.text);
    });
  }
  changeSong() {
    this.played = true;
    this.presentLoading();
    let that = this;
    setTimeout(function(){
      var frame = top.document.querySelector("#iframe12");
      frame["contentWindow"].postMessage(that.song.ytId+"?start=0" ,'*');
      that.loading.dismiss();
    }, 3600);
    
  }

  getLogoFromUrl() {
    this.restProvider.getImageFromUrl("http://radiorolling.com/res/images/20182101446/rolllogo.png")
      .then(data => {
        this.logoSource = data;
        this.changeDetectorRef.detectChanges();
        html2canvas(document.getElementById("shareimg2")).then((canvas)=>{
            let imgData = canvas.toDataURL("image/PNG");
            console.log(imgData);
            this.socialSharing.share("RadioRolling", null, imgData, null);
            this.shareQuote = null;
            this.shareQuoteAuthor = null;
            this.loading.dismiss();
          });
        
      });
  }
  shareSong() {
    this.socialSharing.share("https://youtube.com/watch?v="+this.song.ytId, null, null, null);
  }
  ionViewWillEnter() {
    
  }
  replaceAll (target, search, replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
  };

}
