import {HttpClientModule, HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()

export class YoutubeService {
     googleToken = "AIzaSyAohq-i1Ee_8ei5PLKyMkHYqX9KUDV9Gq8";
     maxResults = 10;
     url = 'https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=';
     constructor(public http: HttpClient) {}
     public getVideos(query:any){
        return this.http.get(this.url+query+'&type=video&order=viewCount&maxResults='+this.maxResults+'&key='+this.googleToken);
     }
     public findVideo(id) {
        return this.http.get("https://www.googleapis.com/youtube/v3/videos?part=snippet,fileDetails,recordingDetails,topicDetails,contentDetails,player,statistics,status&id="+id+"&key="+this.googleToken);
     }
}