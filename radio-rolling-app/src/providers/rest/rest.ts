import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { List } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiUrl = 'http://radiorolling.com/public';
  //apiUrl = 'http://192.168.1.2:8081/public';
  apiRecommend = this.apiUrl + '/api/video/getRecommendedVideo?type=';
  apiPapers = this.apiUrl + '/api/paper/range?';
  //todo: change to LAZY
  apiPlaylists = this.apiUrl + '/api/playlist/t?type=';
  apiPlaylist = this.apiUrl + '/api/playlist/';
  apiNowRolling = this.apiUrl + '/nowPlaying/';
  apiGetImage = this.apiUrl + "/api/video/getImageFromUrl?imageUrl="

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  getRecomended(type) {
    return new Promise(resolve => {
      this.http.get(this.apiRecommend+type).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getNowPlaying(type) {
    return new Promise(resolve => {
      this.http.get(this.apiNowRolling+type).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getPlaylists(type) {
    return new Promise(resolve => {
      this.http.get(this.apiPlaylists+type).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getImageFromUrl(url) {
    return new Promise(resolve => {
      this.http.get(this.apiGetImage+url,{responseType: 'text'}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getCurrentPlaylist(type) {
    return new Promise(resolve => {
      this.http.get(this.apiPlaylist+type).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getPapersInRange(p,start,max) {
    return new Promise(resolve => {
      this.http.get(this.apiPapers+"p="+p+"&start="+start+"&max="+max).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getRecomendedList() {
    var list = [];
    /*list.push(this.getRecomended(1)); //Pesma
    list.push(this.getRecomended(2)); //Video
    list.push(this.getRecomended(3)); //Tekst
    list.push(this.getRecomended(4)); //Preporuka*/
    //list.push(this.getRecomended(5)); //Citat
  }

}
